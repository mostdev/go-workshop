package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

type Lang struct {
	Name string
	URL  string
}

func main() {
	start := time.Now()

	n := 0

	do(func(lang Lang) {
		n++
		count(lang.Name, lang.URL)
	})

	fmt.Printf("%.2fs total\n", time.Since(start).Seconds())

}

func count(name, url string) {
	start := time.Now()
	r, err := http.Get(url)
	if err != nil {
		fmt.Printf("%s: %s", name, err)
		return
	}
	n, _ := io.Copy(ioutil.Discard, r.Body)
	r.Body.Close()
	fmt.Printf("%s %d [%.2fs]\n", name, n, time.Since(start).Seconds())
}

func do(f func(Lang)) {
	// From here file read
	input, err := os.Open("test.json")
	if err != nil {
		log.Fatal(err)
	}

	//Decode the json file
	dec := json.NewDecoder(input)
	for {
		var lang Lang
		err := dec.Decode(&lang)
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}
		f(lang)
	}
}
