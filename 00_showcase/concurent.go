package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

type Lang struct {
	Name string
	URL  string
}

func main() {
	start := time.Now()

	n := 0
	// We add a channel to allow the goroutine to report back.
	c := make(chan string)

	do(func(lang Lang) {
		n++
		go count(lang.Name, lang.URL, c)
	})

	// Print the output of the different channels. Will
	for i := 0; i < n; i++ {
		fmt.Print(<-c)
	}

	fmt.Printf("%.2fs total\n", time.Since(start).Seconds())

}

func count(name, url string, c chan<- string) {
	// start the timer
	start := time.Now()
	// read url
	r, err := http.Get(url)
	if err != nil {
		// If something goes wrong add problem to channel.
		c <- fmt.Sprintf("%s: %s", name, err)
		return
	}
	// Read the retrieved body and discard it, n stores the bytes read.
	n, _ := io.Copy(ioutil.Discard, r.Body)
	r.Body.Close()
	// Get time it took to read body.
	dt := time.Since(start).Seconds()
	// Put the response on the channel
	c <- fmt.Sprintf("%s %d [%.2fs]\n", name, n, dt)
}

func do(f func(Lang)) {
	// From here file read
	input, err := os.Open("test.json")
	if err != nil {
		log.Fatal(err)
	}

	//Decode
	dec := json.NewDecoder(input)
	for {
		var lang Lang
		err := dec.Decode(&lang)
		if err != nil {
			if err == io.EOF {
				break
			}
			log.Fatal(err)
		}
		f(lang)
	}
}
